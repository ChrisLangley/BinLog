FROM elixir:1.4
ADD . /app
WORKDIR /app
RUN mix local.hex --force && mix deps.get --force && mix compile
