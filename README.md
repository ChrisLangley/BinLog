# BinLog

This library is a simple high level wrapper around :disk_log module. It *currently* only exposes a high level interface for my specific use case.

Example:
```
BinLog.open("foo")
{:ok, :foo}
BinLog.write("foo", :complex)
:ok
BinLog.from("foo") |> Stream.each(fn(x) -> IO.inspect(x) end) |> Stream.run
```


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `binlog` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:binlog, "~> 0.1.0"}]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/binlog](https://hexdocs.pm/binlog).

