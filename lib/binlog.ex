defmodule BinLog do
  @moduledoc """
  This module exposes an Elixir interface to :disk_log, read more about :disk_log (http://erlang.org/doc/man/disk_log.htm)

  The interface has been simplified for my use case of writing messages and reading messages from the log.
  """

  alias :disk_log, as: DiskLog

  @typedoc """
  The t type represents a log handler (String, Atom or Charlist)
  """
  @type t :: String.t | atom | char

  @typedoc """
  Alias all the possible write failures
  """
  @type log_reason :: :no_such_log
                            | :nonode
                            | {:read_only_mode, atom}
                            | {:format_external, atom}
                            | {:blocked_log, atom}
                            | {:full, atom}
                            | {:invalid_header, any}
                            | {:file_error, any, any}

  @spec open(disk_log_opts :: Keyword.t) :: {:ok, atom} | {:error, any}
  def open(disk_log_opts) do
    DiskLog.open(disk_log_opts)
  end

  @doc """
  Easily append messages to the end of the log, the data can be any erlang term

  ```
  BinLog.log("foo", ["bar"])
  BinLog.log("foo", :bar)
  BinLog.log("foo", %{"value" => :bar})
  ```
  """
  @spec log(disk_log :: t, term :: any) :: :ok | {:error, log_reason}
  def log(disk_log, term) do
    disk_log
    |> file_to_log_atom()
    |> DiskLog.log(term)
  end

  @doc """
  basic_opts is a helper config to get you logging quickly, you should look up more information from the
  :disk_log documentation to configure for your specific use case.
  """
  @spec basic_opts(file :: t) :: Keyword.t
  def basic_opts(file) do
    [
      {:name, file |> file_to_log_atom()},
      {:file, file |> to_charlist},
      {:type, :halt},
      {:format, :internal},
      {:mode, :read_write},
      {:linkto, self()}
    ]
  end

  @doc """
  Exposes a stream interface from your log with cursors (Pointer to last read records), retries (if hit eof), timeout (sleep between reads).

  ```
  BinLog.from("foo") |> Stream.each(fn(x) -> IO.inspect(x) end) |> Stream.run()
  ```
  """
  @spec from(disk_log :: t, opts :: Keyword.t) :: Enumerable.t
  def from(disk_log, opts \\ []) do
    log_name = disk_log |> file_to_log_atom()
    retries = Keyword.get(opts, :retries, 5)
    chunk_size = Keyword.get(opts, :chunk_size, 1)
    timer = Keyword.get(opts, :timer, 1000)
    cursor = Keyword.get(opts, :cursor, :start)
    Stream.resource(
      #Start
      fn -> {log_name, cursor, retries} end,
      #Iterator
      fn({log_name, cursor, retry_counter}) ->
        case DiskLog.chunk(log_name, cursor, chunk_size) do
          :eof ->
            cond do
              retry_counter == :infinity ->
                Process.sleep(timer)
                {[], {log_name, cursor, retry_counter}}

              retry_counter > 0 ->
                Process.sleep(timer)
                {[], {log_name, cursor, retry_counter - 1}}

              true ->
                {:halt, {log_name, cursor, 0}}
            end
          {:error, reason} ->
            {:halt, reason}
          {new_cursor, terms} ->
            {terms, {log_name, new_cursor, retries}}
          {new_cursor, terms, badbytes} ->
            {terms, {log_name, new_cursor, retries}}
        end
      end,
      #Cleanup
      fn({log_name, cursor, _retry}) -> {log_name, cursor} end
    )
  end

  defp file_to_log_atom(file) when is_binary(file), do: file |> to_charlist |> :erlang.list_to_atom()
  defp file_to_log_atom(file) when is_atom(file), do: file

end
